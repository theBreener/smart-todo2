// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAgwwYo4dZopNq-sLACjctjIAl-qKl5Egg",
    authDomain: "smarttodo-15dbe.firebaseapp.com",
    databaseURL: "https://smarttodo-15dbe.firebaseio.com",
    projectId: "smarttodo-15dbe",
    storageBucket: "smarttodo-15dbe.appspot.com",
    messagingSenderId: "1083935539555"
  }
};
